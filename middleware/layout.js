export default ({ route, store }) => {
  // Take the last value (latest route child)
  const layout = route.meta.reduce(
    (layout, meta) => meta.layout || layout,
    "MainLayout"
  );

  store.commit("SET_LAYOUT", layout);
};
