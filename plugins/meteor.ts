import simpleDDP from 'simpleddp' // ES6
import ws from 'isomorphic-ws'
import { Plugin } from '@nuxt/types'

// Prototype of connection options
interface Options {
  endpoint: string
  SocketConstructor: any
  reconnectInterval: number
}

// Connection option with actual value
let options: Options = {
  endpoint: 'ws://178.128.127.0:8245/websocket',
  SocketConstructor: ws,
  reconnectInterval: 5000
}

// Create connection
const server = new simpleDDP(options)

// Connection callback
server.on('connected', (): void => {
  console.info('server is connected.')
})
server.on('disconnected', (): void => {
  console.warn('server is disconnected.')
})
server.on('error', (error: any): void => {
  console.error('server error: ', error)
})

/**
 * Make plugin accessible to
 * Vue instance
 * Nuxtjs function, context
 * Vuex Store
 */

// For Vue instance
declare module 'vue/types/vue' {
  interface Vue {
    $meteor: any
  }
}

// For Nuxtjs
declare module '@nuxt/types' {
  // Nuxt functions (asyncData, fetch, plugins, middleware, nuxtServerInit)
  interface NuxtAppOptions {
    $meteor: any
  }

  // Nuxt context
  interface Context {
    $meteor: any
  }
}

// For Vuex Store
declare module 'vuex/types/index' {
  interface Store<S> {
    $meteor: any
  }
}

/**
 * Meteor ddp plugin
 * @returns {Object} of ddp class after connect to meteor server
 * @see {@link https://github.com/Gregivy/simpleddp}
 */

const meteorPlugin: Plugin = (context, inject): void => {
  inject('meteor', server)
}

export default meteorPlugin
