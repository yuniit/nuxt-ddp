module.exports = {
  root: true,
  env: {
    node: true,
    browser: true
  },
  extends: [
    // see: https://github.com/prettier/eslint-config-prettier/issues/100
    '@nuxtjs',
    'plugin:nuxt/recommended',
    'plugin:prettier/recommended',
    'prettier',
    'prettier/vue',
    '@nuxtjs/eslint-config-typescript'
  ],
  rules: {
    'no-console': 'warn',
    'vue/html-self-closing': 'off',
    'vue/max-attributes-per-line': [
      1,
      {
        singleline: 1,
        multiline: {
          max: 1,
          allowFirstLine: false
        }
      }
    ],
    'vue/html-indent': [
      'error',
      2,
      {
        attribute: 1,
        closeBracket: 0,
        alignAttributesVertically: true,
        ignores: []
      }
    ],
    'prettier.jsxSingleQuote': true,
    'prettier.singleQuote': true
  },
  globals: {
    $nuxt: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
