export const state = () => ({
  layout: "MainLayout"
});

export const mutations = {
  SET_LAYOUT(state, layout) {
    state.layout = layout;
  }
};
